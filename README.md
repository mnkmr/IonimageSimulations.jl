# IonimageSimulations

[![Build Status](https://gitlab.com/mnkmr/IonimageSimulations.jl/badges/master/build.svg)](https://gitlab.com/mnkmr/IonimageSimulations.jl/pipelines)
[![Coverage](https://gitlab.com/mnkmr/IonimageSimulations.jl/badges/master/coverage.svg)](https://gitlab.com/mnkmr/IonimageSimulations.jl/commits/master)

## Introduction

This is a package to simulate photofragment ion images for julialang.


## Installation

Press `]` to start [Pkg REPL mode](https://docs.julialang.org/en/v1/stdlib/Pkg/), then execute:

```
(v1.1) pkg> registry add https://gitlab.com/mnkmr/MNkmrRegistry.git
(v1.1) pkg> add IonimageSimulations
```

## Usage

See [the jupyter notebook](https://nbviewer.jupyter.org/urls/gitlab.com/mnkmr/IonimageSimulations.jl/raw/master/HowToUse.ipynb)
