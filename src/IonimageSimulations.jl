module IonimageSimulations

export
    simulateimg,
    zare,
    rakitzis,
    gauss,
    muckerman

using Photofragments:
    random_particle,
    random_particle!,
    zare,
    rakitzis,
    gauss,
    muckerman

"Simulate a totally projected (non-sliced) ion image."
@inline function totalimage(f::Function, n::Real, height::Real, width::Real, center,
                            rmin::Real, rmax::Real)
    dummymass = -1.0
    p = random_particle(dummymass, 1, rmin, rmax)
    image = zeros(Float64, (height, width))
    totalint = 0.0
    for i in 1:floor(Int, n)
        random_particle!(p, rmin, rmax)
        intensity = f(p)
        totalint += intensity
        y, x = project(p, center)
        if 0 < y <= height && 0 < x <= width
            @inbounds image[y, x] += intensity
        end
    end
    return image.*(n/totalint)
end


"Simulate a sliced ion image."
@inline function slicedimage(f::Function, n::Real, height::Real, width::Real, center,
                             rmin::Real, rmax::Real, smin::Real, smax::Real)
    @assert smin < smax
    dummy = -1.0
    p = random_particle(dummy, 1, rmin, rmax)
    Ωmin = acosd(clamp(smax, -1, 1))
    Ωmax = acosd(clamp(smin, -1, 1))
    image = zeros(Float64, (height, width))
    totalint = 0.0
    for i in 1:floor(Int, n)
        random_particle!(p, rmin, rmax)
        intensity = f(p)
        totalint += intensity
        !(Ωmin <= p.Ω <= Ωmax) && continue
        y, x = project(p, center)
        if 0 < y <= height && 0 < x <= width
            @inbounds image[y, x] += intensity
        end
    end
    return image.*(n/totalint)
end


"Calculate a projected photofragment position on a screen."
@inline function project(p, center)
    yc, xc = center
    v = p.v
    Ω = p.Ω
    Θ = p.Θ
    y = round(Int, yc - v*sind(Ω)*cosd(Θ), RoundNearestTiesUp)
    x = round(Int, xc + v*sind(Ω)*sind(Θ), RoundNearestTiesUp)
    return y, x
end


"""
    simulateimg(f::Function, n::Real;
                slice::Real=1.0, smin::Real=-slice, smax::Real=slice,
                size=(501, 501), center=@.((size+1)/2),
                rmin::Real=0, rmax::Real=(minimum(size)-1)/2)

Simulate an image of n photofragments.

# Arguments
- f: photofragment distribution function; f(p::Photofragment)::Float64
- n: number of photofragments to simulate
- rmin: the minimum limit of fragment speed (radius) to take into account in pixel
- rmax: the maximum limit of fragment speed (radius) to take into account in pixel
- slice: slicing width fraction to the diameter of Newton sphere
         1.0 for a totally projected image
         0.1 for an ideal (and typical) slice image
- smin,smax: fine control of slicing range, it ranges from -1.0 to 1.0
- size: size of screen (height, width) in pixcel
- center: center coordinate of the projection (y, x)
"""
function simulateimg(f::Function, n::Real, rmin::Real, rmax::Real,
                     smin::Real, smax::Real, size, center)
    height, width = size
    if smin <= -1.0 && 1.0 <= smax
        img = totalimage(f, n, height, width, center, rmin, rmax)
    else
        img = slicedimage(f, n, height, width, center, rmin, rmax, smin, smax)
    end
    return img
end
function simulateimg(f::Function, n::Real;
                     slice::Real=1.0, smin::Real=-slice, smax::Real=slice,
                     size=(501, 501), center=@.((size+1)/2),
                     rmin::Real=0, rmax::Real=(minimum(size)-1)/2)
    return simulateimg(f, n, rmin, rmax, smin, smax, size, center)
end
"""
        simulateimg(n::Real, μ::Real, σ::Real, βarray; kwargs...)

Simulate an image of n photofragments with a Gaussian speed distribution
G(μ, σ) and an Zare's equation of angular distribution
    I(θ) = 1 + β₂P₂(cosθ) + ...
where Pₙ(cosθ) is the n-th order Legendre polynomial.

`βarray` should be an array of iterable object holding two items, an order of
Legendre polynomial and an associated anisotropy value; for example
[(2, 2.0), (4, 0.5)], this example compute I(θ) = 1 + β₂P₂(cosθ) + β₄P₄(cosθ)
with β₂ = 2.0 and β₄ = 0.5.
"""
function simulateimg(n::Real, μ::Real, σ::Real, βarray; kwargs...)
    if σ ≈ 0.0
        f = p -> 1
    else
        f = p -> gauss(p.v, μ, σ)*zare(p, βarray...)
    end
    rmin, rmax = μ - 3σ, μ + 3σ
    return simulateimg(f, n; rmin=rmin, rmax=rmax, kwargs...)
end
"""
        simulateimg(n::Real, μ::Real, σ::Real, β₂::Real=2.0; kwargs...)

Simulate an image of n photofragments with a Gaussian speed distribution
G(μ, σ) and an Zare's equation of angular distribution
    I(θ) = 1 + β₂P₂(cosθ)
where Pₙ(cosθ) is the n-th order Legendre polynomial.
"""
simulateimg(n::Real, μ::Real, σ::Real, β₂::Real=2.0; kwargs...) =
    simulateimg(n::Real, μ::Real, σ::Real, [2 => β₂]; kwargs...)


end # module
